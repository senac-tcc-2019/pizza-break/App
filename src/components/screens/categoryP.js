import React, {Component} from "react";
import { TouchableHighlight, StyleSheet, Text, View, ScrollView, Image, Button } from "react-native";
import api from '../../services/api'
import { withNavigation } from 'react-navigation';
import { AsyncStorage } from 'react-native'

class categoryP extends Component {

    state = {
        products: [],
        id: '',
        prod: [],
        erro: '',
        carrinho: [],
        add: '',
        mostra: false,
        adicionado: [],
        quem: '',
        finaliza: ''
    }

    async componentDidMount(){
        const id = this.props.navigation.state.params.id;
        const product = await api.get('/auth/product')
        if(product.data){
            this.setState({
                products: product.data,
                id: id
            })
            console.log(this.state.products)
        }else {
            this.setState({erro: 'Sem Produtos Disponiveis no Momento!'})
        }
    }

   async handleAddOrder(id) {
        const quem = await api.post(`/consultaQuem`,{
            product_id:  id
        })

        this.state.adicionado.push(id);

        this.setState({
            Add: 'Produto Adicionado!',
            mostra: true,
            quem: quem
        })
    }

    async handleFinaliza(){
        await AsyncStorage.setItem('seusProdutos', JSON.stringify(this.state.adicionado) )
        const pega = await AsyncStorage.getItem('seusProdutos')
        console.log(pega)

        this.setState({finaliza: 'Pedido Finalizado! Aguarde.'})
        this.props.navigation.navigate('Produtos')
    }

    render(){
        const { products, id } = this.state;
        console.log(this.state.adicionado)
        return(
           <ScrollView>
           {products.map(el => (
               el.category_id === id ? 
               <View>
                   <TouchableHighlight 
                    key={el.id}>
                            <Image 
                          source={{uri: el.image[0].url}}
                          style={styles.item}
                        /> 

                     </TouchableHighlight>
                     <Button
                           title="Adicionar no Carrinho"
                           onPress={() => this.handleAddOrder(el.id)}
                       />
               </View>
            : <Text>.</Text>
           ))}
                {this.state.mostra === true ?
                    <View style={styles.fixToText}>
                        <Button
                            title="Finalizar Pedido(s)"
                            onPress={() => this.handleFinaliza }
                        /> 
                    </View>
                :  <Text></Text>
                }
    <Text>{this.state.finaliza}</Text>
         </ScrollView>
        )
    }
}

export default withNavigation(categoryP)

const styles = StyleSheet.create({
    item: {
      alignItems: "center",
      backgroundColor: "rgba(199, 21, 133, 0.56)",
      flexBasis: 0,
      flexGrow: 1,
      margin: 6,
      top: 30,
      bottom: 40,
      padding: 80,
      height: 60,
      borderRadius: 10
    },
    fixToText: {
        flexDirection: 'row',
        justifyContent: 'space-between'
      }
  });
  