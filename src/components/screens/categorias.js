import React from "react";
import { TouchableHighlight, StyleSheet, Text, View, ScrollView, Image } from "react-native";
import api from '../../services/api'
import { createStackNavigator, createAppContainer, withNavigation } from 'react-navigation';
import categoryP from "./categoryP";


class Categoria extends React.Component {
  state = {
    data: []
  }

  AppNavigator = createStackNavigator(
    {
      Produtos: {
        screen: categoryP
      }
    }
  );

  AppContainer = createAppContainer(this.AppNavigator);

  async componentDidMount() {
    try {
      const response = await api.get('/auth/category/');
      if(response){
        this.setState({
          data: response.data
        })
        console.log(response.data.image)
      }else {
        console.log("error undefined categorias")
      }
    } catch (error) {
      console.log(error)
    }
  }


 render() {
    const { data } = this.state;
    return (
        <ScrollView>
          {data.map(el => (
            <View>
                <TouchableHighlight 
                  key={el.id}
                  onPress={() => this.props.navigation.navigate('Produtos', {id: el.id}) }
                >
                  <Image 
                      source={{uri: el.image[0].url}}
                      style={styles.item}
                    />
                  </TouchableHighlight>
            </View>
          ))}
        </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  item: {
    alignItems: "center",
    backgroundColor: "rgba(199, 21, 133, 0.56)",
    flexBasis: 0,
    flexGrow: 1,
    margin: 6,
    top: 30,
    bottom: 40,
    padding: 80,
    height: 60,
    borderRadius: 10
  },
  text: {
    color: "#fff"
  }
});

export default withNavigation(Categoria);