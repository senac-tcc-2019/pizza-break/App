import React, { Component } from 'react'
import { StyleSheet, Text, View, ScrollView} from "react-native";
import api from '../../services/api'

export default class Pedidos extends Component {
  state = {
    pedidos: [],
    id: 1,
    orders: []
  }

  async componentDidMount(){
    const user = 1;
    const dados = await api.get('/auth/order')
    if(dados.data){
      this.setState({
        orders: dados.data
      })
      // console.log(this.state.orders)
    }
    // this.handleMeus();
  }

  // handleMeus(){
  //   this.state.orders.map((el) => {
  //     if(el.user_id === this.state.id){
  //       this.state.pedidos.push(el)
  //     }
  //     console.log(this.state.pedidos)
  //   })
  // }

  render() {
    const { orders } = this.state
    return (
      <ScrollView>
        <View style={styles.textContent}>
        {orders.map((el) => (
           this.state.id === el.user_id ?
           <View key={el.id} style={styles.item}>
              <Text style={{color: '#fff'}}>Numero Pedido: {el.id}</Text>
              <Text style={{color: '#fff'}}>Troco: {el.change}</Text>
              <Text style={{color: '#fff'}}>Status: {el.motivo}</Text>
              <Text style={{color: '#fff'}}>Pagamento: {el.forma}</Text>
           </View>
         : ''
        ))}
        </View>
    </ScrollView>

    )
  }
}

const styles = StyleSheet.create({
  item: {
    alignItems: "center",
    backgroundColor: "#f72964",
    margin: 4,
    padding: 60,
    top: 30,
    bottom: 30,
    height: 120,
    borderRadius: 10
 
    },
    textContent: {
      padding: 3,
      marginTop: 10,
      height: 600
    }
  });