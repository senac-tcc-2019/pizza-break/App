import React, { Component } from 'react'
import { TextInput, Image, StyleSheet, View, Text} from 'react-native'
import api from '../../services/api'


  handleChange = (e) => {
    e.onChangeText
  }


export default class User extends Component{
    state = {
        nome: '',
        email: '',
        image: '',
        telefone: '',
        address: '',
        number: '',
        neighborhood: '',
        reference: '',
        loggedInUser: ''
    }
    
    async componentDidMount() {

        try {
          const { id } = 1
          const res = await api.get(`/auth/user/1`)

            this.setState({
              nome: res.data.name,
              email: res.data.email,
              image: res.data.images[0].url,
              telefone: res.data.telephone,
              address: res.data.address,
              number: res.data.number,
              neighborhood: res.data.neighborhood,
              reference: res.data.reference
            })
            console.log(this.state.nome + ' - ' + this.state.email + ' - ' + this.state.image)

        } catch (err) {
         
        }
  
    }

    render() {
        return(
            <View style={Styles.textContent}>
                    <Image 
                      source={{uri: this.state.image}}
                      style={{height: 200, width: 300, borderRadius: 10}}
                    />
                <Text style={{ fontSize: 20 }}>Nome: {this.state.nome}</Text>
                <Text style={{ fontSize: 15 }}>Email: {this.state.email}</Text>
                <Text style={{ fontSize: 15 }}>Fone: {this.state.telefone}</Text>
                <Text style={{ fontSize: 15 }}>End: {this.state.address}</Text>
                <Text style={{ fontSize: 15 }}>Numero: {this.state.number}</Text>
                <Text style={{ fontSize: 15 }}>Bairro: {this.state.neighborhood}</Text>
                <Text style={{ fontSize: 15 }}>Referência: {this.state.reference}</Text>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    textContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})