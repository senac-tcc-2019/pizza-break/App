import { createStackNavigator } from 'react-navigation'

import SignIn from './components/signIn'
import SignUp from './components/signUp'
import Home from './components/home'
import Produtos from './components/screens/categoryP'
import Carrinho from './components/screens/carrinho'

const Routes = createStackNavigator(
  {
    SignIn,
    SignUp,
    Home,
    Produtos,
    Carrinho,
  },{
    header: null,
    headerMode: 'none',
    initialRouteName: 'SignIn'
  })

export default Routes
